﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Sementes.Models
{
    public class SementesContext : DbContext
    {
        public SementesContext (DbContextOptions<SementesContext> options)
            : base(options)
        {
        }

        public DbSet<Sementes.Models.Medicamento> Medicamento { get; set; }
    }
}
